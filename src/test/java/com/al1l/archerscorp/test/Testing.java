package com.al1l.archerscorp.test;

import cl.archerscorp.core.chat.Formatter;
import cl.archerscorp.core.http.HTTP;
import com.google.common.util.concurrent.ListenableFuture;
import org.junit.Test;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class Testing {

    @Test
    public void formatting() {
        Formatter f = new Formatter(null);
        System.out.println("\"" + f.getWidth("TEST") + "\"");
        System.out.println("\"" + f.centerString("TEST") + "\"");
        System.out.println("\"" + f.rainbowifyString("TEST") + "\"");
    }

    @Test
    public void httpGet() {
        ListenableFuture<String> out = HTTP.sendGet("https://www.theartex.net/cloud/api/?sec=announcements", "Example-CoHTTP/1.0");

        // Add a listener to wait for response
        out.addListener(() -> {
            try {
                // Get response with `out.get()`
                String response = out.get();
                System.out.println(response);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }, Runnable::run);
    }

    @Test
    public void httpPost() {

        // Create POST parameters
        HashMap<String, String> postParametets = new HashMap<>();
        postParametets.put("sec", "announcements");

        // Create POST request
        ListenableFuture<String> out = HTTP.sendPost("https://www.theartex.net/cloud/api/", postParametets, "Example-CoHTTP/1.0");

        // Add a listener to wait for response
        out.addListener(() -> {
            try {
                // Get response with `out.get()`
                String response = out.get();
                System.out.println(response);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }, Runnable::run);
    }
}
