package cl.archerscorp.core.config.common;

import cl.archerscorp.core.config.IConfig;
import cl.archerscorp.core.config.IEntry;
import cl.archerscorp.core.config.ISection;
import cl.archerscorp.core.plugin.ArcherPlugin;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Section implements ISection {

    private final String path;
    private final IConfig config;
    private final ArcherPlugin plugin;

    public Section(String path, IConfig config, ArcherPlugin plugin) {
        this.path = path;
        this.config = config;
        this.plugin = plugin;
    }

    @Override
    public Set<String> getKeys(boolean deep) {
        Set<String> keys = new HashSet<>();
        if (deep) {
            config.getEntries().stream().filter(e -> e.getPath().startsWith(path + ".")).forEach(e -> keys.add(e.getPath()));
        } else config.getEntries().stream().filter(e -> e.getPath().startsWith(path + ".")).forEach(e -> {
            String key = e.getPath().substring(path.length() + 1);
            int size = key.indexOf(".");
            if (size < 0) size = key.length();
            keys.add(key.substring(0, size));
        });
        return keys;
    }

    @Override
    public Set<IEntry> getEntries() {
        Set<IEntry> entries = new HashSet<>();
        config.getEntries().stream().filter(e -> e.getPath().startsWith(path + ".")).forEach(entries::add);
        return entries;
    }

    @Override
    public ISection getSection(String path) {
        return new Section(this.path + "." + path, config, plugin);
    }

    @Override
    public boolean contains(String path) {
        return getKeys(false).contains(path);
    }

    @Override
    public String getCurrentPath() {
        return path;
    }

    @Override
    public String getName() {
        return path.substring(path.lastIndexOf("."));
    }

    @Override
    public IConfig getConfig() {
        return config;
    }

    @Override
    public ArcherPlugin getPlugin() {
        return plugin;
    }

    @Override
    public IEntry getEntry(String path) {
        return config.getEntry(this.path + "." + path);
    }


    @Override
    public String getString(String path) {
        return getEntry(path).getString();
    }

    @Override
    public double getDouble(String path) {
        return getEntry(path).getDouble();
    }

    @Override
    public int getInt(String path) {
        return getEntry(path).getInt();
    }

    @Override
    public long getLong(String path) {
        return getEntry(path).getLong();
    }

    @Override
    public boolean getBoolean(String path) {
        return getEntry(path).getBoolean();
    }

    @Override
    public List<?> getList(String path) {
        return getEntry(path).getList();
    }

    @Override
    public List<String> getStringList(String path) {
        return getEntry(path).getStringList();
    }

    @Override
    public Object getValue(String path) {
        return getEntry(path).getValue();
    }

    @Override
    public ISection getParent() {
        if (!path.contains(".")) return null;
        return new Section(path.substring(0, path.lastIndexOf(".")), config, plugin);
    }
}
