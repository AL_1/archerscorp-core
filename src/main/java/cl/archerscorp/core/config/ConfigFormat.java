package cl.archerscorp.core.config;

public enum ConfigFormat {
    JSON,
    YAML;

}
