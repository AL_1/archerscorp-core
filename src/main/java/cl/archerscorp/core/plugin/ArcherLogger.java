package cl.archerscorp.core.plugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ArcherLogger {

    private final Logger log;
    private final ConsoleCommandSender sender;
    private final ArcherPlugin plugin;

    public ArcherLogger(ArcherPlugin plugin) {
        this.plugin = plugin;
        this.log = plugin.getServer().getLogger();
        this.sender = Bukkit.getConsoleSender();
    }

    public void log(Level level, String msg) {
        if (level == Level.INFO) {
            msg = plugin.getPluginColor() + "[" + plugin.getName() + "] " + ChatColor.RESET + msg;
            sender.sendMessage(msg);
        } else {
            msg = "[" + plugin.getName() + "] " + msg;
            log.log(level, msg);
        }
    }

    public void warn(String msg) {
        log(Level.WARNING, msg);
    }

    public void error(String msg) {
        log(Level.WARNING, msg);
    }

    public void info(String msg) {
        log(Level.INFO, msg);
    }

    public void severe(String msg) {
        log(Level.SEVERE, msg);
    }

    public void log(String msg) {
        log(Level.INFO, msg);
    }

    public void config(String msg) {
        log(Level.CONFIG, msg);
    }

    public void fine(String msg) {
        log(Level.FINE, msg);
    }

    public void finer(String msg) {
        log(Level.FINER, msg);
    }

    public void finest(String msg) {
        log(Level.FINEST, msg);
    }
}
