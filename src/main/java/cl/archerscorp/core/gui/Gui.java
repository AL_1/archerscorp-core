package cl.archerscorp.core.gui;

import cl.archerscorp.core.CorePlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Gui implements Listener {

    private List<GuiElement> guiElements = new ArrayList<>();
    private String title = "GUI";
    private int size = 0;
    private List<Player> players = new ArrayList<>();
    private Inventory inventory;
    private Inventory oldInventory;
    private boolean isOpen = false;
    private GuiEvent onCloseEvent;
    private boolean rebuilding = false;
    private boolean autoSize = false;
    private ItemStack filler = null;
    private List<Integer> filledSlots = new ArrayList<>();

    public Gui() {
        Bukkit.getPluginManager().registerEvents(this, CorePlugin.getInstance());
    }

    public static int pointToSlot(Point point) {
        int slot = (point.y * 9) + point.x;
        return slot;
        // 53 = (5*9)+x
    }

    public static Point slotToPoint(int s) {
        int y = (int) Math.floor(s / 9);
        int x = s - (y * 9);
        return new Point(x, y);
    }

    public static List<Point> shapeToPoints(Rectangle shape) {
        List<Point> points = new ArrayList<>();
        for (int x = shape.x; x < shape.width + shape.x; x++) {
            if (x > 8)
                continue;
            for (int y = shape.y; y < shape.height + shape.height; y++) {
                points.add(new Point(x, y));
            }
        }
        return points;
    }

    public static List<Integer> shapeToSlots(Rectangle shape) {
        List<Integer> points = new ArrayList<>();
        for (int x = shape.x; x < shape.width + shape.x; x++) {
            if (x > 8)
                continue;
            for (int y = shape.y; y < shape.height + shape.y; y++) {
                points.add(pointToSlot(new Point(x, y)));
                Collections.sort(points);
            }
        }
        return points;
    }

    public static Rectangle getShape(Point p1, Point p2) {
        Rectangle shape = new Rectangle();
        shape.setFrameFromDiagonal(p1, p2);
        return shape;
    }

    public void setRebuilding(boolean rebuilding) {
        this.rebuilding = rebuilding;
    }

    public Gui onClose(GuiEvent event) {
        onCloseEvent = event;
        return this;
    }

    public Gui add(GuiElement element) {
        guiElements.add(element);
        element.setGui(this);
        return this;
    }

    public Gui addAll(List<GuiElement> elements) {
        guiElements.addAll(elements);
        for (GuiElement e : elements)
            e.setGui(this);
        return this;
    }

    public Gui remove(GuiElement element) {
        guiElements.remove(element);
        element.setGui(null);
        return this;
    }

    public Gui removeAll(List<GuiElement> elements) {
        guiElements.removeAll(elements);
        for (GuiElement e : elements)
            e.setGui(null);
        return this;
    }

    public ItemStack getFiller() {
        return filler;
    }

    public void setFiller(ItemStack filler) {
        this.filler = filler;
    }

    public Gui build() {
        Inventory inv = Bukkit.createInventory(null, size, title);
        for (GuiElement e : guiElements) {
            e.onPlace(inv);
        }
        filledSlots = new ArrayList<>();
        for (GuiElement elmt : guiElements)
            filledSlots.addAll(shapeToSlots(elmt.shape));
        if (filler != null)
            for (int i = 0; i < size; i++)
                if (!filledSlots.contains(i))
                    inv.setItem(i, filler);
        this.oldInventory = this.inventory;
        this.inventory = inv;
        if (size != inv.getSize() && autoSize)
            return build();
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Gui setTitle(String title) {
        this.title = title;
        if (isOpen)
            reopen();
        return this;
    }

    public int getSize() {
        return size;
    }

    public Gui setSize(int size) {
        this.size = size;
        if (isOpen)
            reopen();
        return this;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public boolean isAutoSize() {
        return autoSize;
    }

    public void setAutoSize(boolean autoSize) {
        this.autoSize = autoSize;
    }

    public Gui reopen() {
        rebuilding = true;
        if (isOpen) {
            //for (Player p : players)
            //    if (p.getOpenInventory() != null)
            //        if (p.getOpenInventory().getTopInventory().equals(inventory))
            //            p.closeInventory();
            build();
            for (Player p : players)
                p.openInventory(inventory);
        } else {
            build();
            for (Player p : players)
                p.openInventory(inventory);
            isOpen = true;
        }
        rebuilding = false;
        return this;
    }

    public Gui open(Player... ps) {
        if (!isOpen)
            build();
        for (Player p : ps)
            if (p != null) {
                players.add(p);
                p.openInventory(inventory);
            }
        isOpen = true;
        return this;
    }

    public Gui close(Player p) {
        if (players.contains(p)) {
            players.remove(p);
            if (p.getOpenInventory() != null)
                if (p.getOpenInventory().getTopInventory().equals(inventory))
                    p.closeInventory();
        }
        if (players.size() == 0)
            isOpen = false;
        return this;
    }

    public Gui closeAll() {
        List<Player> ps = new ArrayList<>();
        ps.addAll(this.players);
        for (Player p : ps)
            close(p);
        isOpen = false;
        return this;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getView() == null)
            return;
        if (e.getView().getTopInventory().equals(oldInventory)) {
            e.setCancelled(true);
            return;
        }
        if (e.getView().getTopInventory().equals(inventory)) {
            e.setCancelled(true);
            if (e.getClickedInventory() == null)
                return;
            if (e.getClickedInventory().equals(inventory))
                for (GuiElement elmt : guiElements) {
                    List<Integer> slots = shapeToSlots(elmt.shape);
                    if (slots.contains(e.getSlot())) {
                        if (!elmt.isReadOnly())
                            e.setCancelled(false);
                        elmt.onClick(e);
                        return;
                    }
                }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getView() != null)
            if (e.getView().getTopInventory() != null)
                if (!rebuilding)
                    if (e.getView().getTopInventory().equals(inventory))
                        if (e.getPlayer() != null)
                            if (onCloseEvent != null)
                                onCloseEvent.on(this, (Player) e.getPlayer());
    }

    public List<GuiElement> getGuiElements() {
        List<GuiElement> elements = new ArrayList<>();
        elements.addAll(guiElements);
        return elements;
    }


    /*
    0  1  2  3  4  5  6  7  8
    9  10 11 12 13 14 15 16 17
    18 19 20 21 22 23 24 25 26
    27 28 29 30 31 32 33 34 35
    36 37 38 39 40 41 42 43 44
    45 46 47 48 49 50 51 52 53

    1  2  3  4  5  6  7  8  9
    10 11 12 13 14 15 16 17 18
    19 20 21 22 23 24 25 26 27
    28 29 30 31 32 33 34 35 36
    37 38 39 40 41 42 43 44 45
    46 47 48 49 50 51 52 53 54
     */
}