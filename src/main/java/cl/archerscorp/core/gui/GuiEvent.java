package cl.archerscorp.core.gui;

import org.bukkit.entity.Player;

@FunctionalInterface
public interface GuiEvent {
    public void on(Gui gui, Player player);
}
