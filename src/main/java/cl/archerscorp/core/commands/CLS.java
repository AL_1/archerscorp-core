package cl.archerscorp.core.commands;

import cl.archerscorp.core.CorePlugin;
import cl.archerscorp.core.command.ArcherCmd;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.command.CommandSenders;

import java.util.List;

public class CLS extends ArcherCommand {

    public CLS() {
        super(CorePlugin.getInstance(), "CLS");
        setMaxArgs(0);
        setAllowedSenders(CommandSenders.CONSOLE);
    }

    @Override
    public void onCommand(ArcherCmd cmd) {
        cmd.getSender().sendMessage("Use the command '/cmd cls' instead.");
    }

    @Override
    public List<String> onTabComplete(ArcherCmd cmd) {
        return null;
    }
}
