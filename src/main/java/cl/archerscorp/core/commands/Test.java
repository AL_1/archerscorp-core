package cl.archerscorp.core.commands;

import cl.archerscorp.core.CorePlugin;
import cl.archerscorp.core.command.ArcherCmd;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.command.CommandSenders;

import java.util.List;

public class Test extends ArcherCommand {

    public Test() {
        // Plugin instance, command name, description, ...aliases
        super(CorePlugin.getInstance(), "test", "Just a test command :P", "t");

        // Only allow players
        setAllowedSenders(CommandSenders.PLAYER);
    }

    @Override
    public void onCommand(ArcherCmd cmd) {
        cmd.getPlayerSender().sendMessage("Test success!");
    }

    @Override
    public List<String> onTabComplete(ArcherCmd cmd) {
        return null;
    }
}

/*
        cmd.getSender().sendMessage("Opening gui...");
        new Gui().add(new Button(new ItemBuilder(Material.STICK).displayName("Am stick").build()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                cmd.getSender().sendMessage("Clicked Stick");
                getGui().setTitle(System.currentTimeMillis() + "");
                getGui().setSize(getGui().getSize() - 9);
            }
        }.setSize(6, 9)).onClose((gui, player) -> {
            cmd.getSender().sendMessage("Gui closed");
        }).open(cmd.getPlayerSender());
 */