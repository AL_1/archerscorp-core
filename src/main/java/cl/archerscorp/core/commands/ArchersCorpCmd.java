package cl.archerscorp.core.commands;

import cl.archerscorp.ArchersCorp;
import cl.archerscorp.core.CorePlugin;
import cl.archerscorp.core.command.ArcherCmd;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.command.CommandSenders;
import cl.archerscorp.core.gui.Button;
import cl.archerscorp.core.gui.Gui;
import cl.archerscorp.core.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.List;

public class ArchersCorpCmd extends ArcherCommand {

    public ArchersCorpCmd() {
        super(CorePlugin.getInstance(), "archerscorp", "ArchersCorp main command", "ac");
        setAllowedSenders(CommandSenders.PLAYER);
    }

    @Override
    public void onCommand(ArcherCmd cmd) {
        Gui g = new Gui();
        g.setTitle(ChatColor.BLUE + "ArchersCorp");
        g.setAutoSize(true);

        if (cmd.getSender().hasPermission("archerscorp.moderate") && ArchersCorp.getPlugin("Admin") != null)
            g.add(new Button(new ItemBuilder(Material.SKULL_ITEM).durability(3).displayName(ChatColor.RED + "Moderate").build()) {
                @Override
                public void onClick(InventoryClickEvent e) {
                    g.closeAll();
                    cmd.getPlayerSender().performCommand("/mod");
                }
            });

        g.open(cmd.getPlayerSender());
    }

    @Override
    public List<String> onTabComplete(ArcherCmd cmd) {
        return null;
    }
}
