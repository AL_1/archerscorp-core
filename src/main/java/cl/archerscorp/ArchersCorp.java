package cl.archerscorp;

import cl.archerscorp.core.CorePlugin;
import cl.archerscorp.core.chat.Formatter;
import cl.archerscorp.core.command.ArcherCommand;
import cl.archerscorp.core.plugin.ArcherLogger;
import cl.archerscorp.core.plugin.ArcherPlugin;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

public class ArchersCorp {

    private static final List<ArcherCommand> registeredCommands = new ArrayList<>();

    public static boolean registerCommand(ArcherCommand command) {
        if (!registeredCommands.contains(command))
            registeredCommands.add(command);
        if (CorePlugin.getInstance().isEnabled() && !command.isRegistered()) {
            try {
                command.register();
                log().info("Registered command " + command.getName());
                return true;
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
                log().warn("Failed to register command " + command.getName());
            }
        }
        return false;
    }

    public static boolean unregisterCommand(ArcherCommand command) {
        if (registeredCommands.contains(command))
            registeredCommands.remove(command);
        if (CorePlugin.getInstance().isEnabled() && command.isRegistered()) {
            try {
                command.unregister();
                log().info("Unregistered command " + command.getName());
                return true;
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
                log().warn("Failed to unregister command " + command.getName());
            }
        }
        return false;
    }

    public static ArcherLogger log() {
        return CorePlugin.getInstance().log();
    }

    public static Formatter format() {
        return CorePlugin.getInstance().format();
    }

    public static List<ArcherCommand> getRegisteredCommands() {
        return registeredCommands;
    }

    public static ArcherPlugin getPlugin(String name) {
        return (ArcherPlugin) Bukkit.getPluginManager().getPlugin("ArchersCorp-" + name);
    }
}
